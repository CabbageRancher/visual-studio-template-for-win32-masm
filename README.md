
**This is intended only for Professor Nowlin's Computer Architecture and Design course.**


# Visual Studio Template for WIN32 MASM

A blank template for creating WIN32 Intel assembly language apps in Visual Studio

## Getting started

The Irvine folder should be extracted to *C:\Irvine*. If you do not want this, update the location in Microsoft MacroAssembler and additional locations in the linker settings of the project properties.

Navigate to the following Folder:
```
C:\Users\<username>\Documents\Visual Studio 20<XX>\Templates\ProjectTemplates
```
Create a folder to store your template *MASMProject32.zip*. (Do not unzip this folder.) Suggestions include something along the lines of "MASM" or "Assembly" to keep things organized.

Close Visual Studio if it is open. Open Visual Studio and create a new project using the type template you just added. If you do not see it in the list of options, use the search bar.

It is recommended to select the option **Place solution and project in the same folder** but not required.

You should now have a *BlankMain.asm* file that can be renamed. You can use this as the starting point for your project.

Processor directives and such will come from the included *SmallWin.inc* that is included in *irvine32.inc*.

Good luck on your projects!
